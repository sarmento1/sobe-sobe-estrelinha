(function($) {
    $myr = $('.my-rating');
    theBlog  = $myr.data('blog-id');
    thePost  = $myr.data('post-id');
    theAPI   = $myr.data('api');
    theRate  = $myr.data('rating');
    theCount = $myr.data('counter');
    theHigh  = $myr.data('high-demand');

    if ('NaN' == theRate) theRate = 0;

    function ajaxsetup(){
        $.ajaxSetup({
            type: 'GET',
            async: true,
            beforeSend:
                function (xhr) {
                  if (xhr && xhr.overrideMimeType) {
                    xhr.overrideMimeType('application/json;charset=utf-8');
                  }
                },
            dataType: 'json',
        })
    }

    function showStats(mustforce){
        if (!mustforce && theHigh == 1) {
            $('#ratingaverage').html(theRate);
            $('#ratingcount').html(theCount);
        }
        else {
            $.ajax({
                url: theAPI + '/stats/' + theBlog + '/' + thePost,
                success: function(d){
                    $('#ratingaverage').html(d.average);
                    $('#ratingcount').html(d.count);
                    theRate = d.average;
                }
            });
        }
    }

    ajaxsetup();
    showStats(false);

    $myr.starRating({
        starSize: 32,
        useFullStars: true,
        initialRating: theRate,
        callback: function(currentRating, $el){
            url = theAPI + '/vote/'+theBlog+'/'+thePost+'/'+currentRating;
            $.ajax({
                url: url,
                success: function(d){ showStats(true); }
            });
        }
    });        
})( jQuery );
