<?php
/**
 * Plugin Name: Sobe Sobe Estrelinha
 * Plugin URI: https://lucrandonarede.com.br/avaliacoes-com-estrelinhas-feitas-do-jeito-certo/
 * Description: Estrelinhas do jeito certo e muito mais
 * Author: Janio Sarmento
 * Author URI: https://janio.sarmento.org
 * Version: 0.9
 */

define('TRANSIENT_SITE_ID', 'pfrating_site_id');
define('PF_UASTRING', 'Firefox 60');

function sse_getSiteID()
{
    $options = get_option('sse_settings');
    $wproptions = array('timeout' => 20, 'user-agent' => PF_UASTRING);
    $value = get_transient(TRANSIENT_SITE_ID);
    if (false === $value) {
        $ep = $options['sse_api_endpoint'];
        $urlparts = parse_url(site_url());
        $domain = $urlparts['host'];
        $reqSite = wp_remote_get($ep . '/site/' . $domain);
        $o = json_decode(wp_remote_retrieve_body($reqSite));
        $blogID = $o->id;

        set_transient(TRANSIENT_SITE_ID, $blogID);
    } else {
        $blogID = $value;
    }

    return $blogID;
}

function pfRating($content)
{
    $options = get_option('sse_settings');
    $ep = $options['sse_api_endpoint'];
	$highdemand = $options['sse_highdemand'];
    $postID = get_the_ID();
    $override = get_post_meta($postID, 'sse_override', true);

    if ($override == 'S') {
        $exibe = true;
    } elseif ($override == 'N') {
        $exibe = false;
    } else {
        if (is_single() && '1' === $options['sse_show_in_posts']) {
            $exibe = true;
        }

        if (is_page() && '1' === $options['sse_show_in_pages']) {
            $exibe = true;
        }

    }

    if ($ep) { // endpoint definido
        if ($exibe) { // é pra exibir
            $blogID = sse_getSiteID();

            if ($blogID) {
                $reqStat = wp_remote_get($ep . "/stats/{$blogID}/{$postID}", $wproptions);
                $o = json_decode(wp_remote_retrieve_body($reqStat));
                $scheme = array(
                    "@context" => "https://schema.org/",
                    "@type" => "CreativeWorkSeries",
                    "name" => get_the_title(),
                    "aggregateRating" => array(
                        "@type" => "AggregateRating",
                        "ratingValue" => $o->average,
                        "bestRating" => $o->best,
                        "ratingCount" => $o->count,
                    ),
                );
                $h = '<script type="application/ld+json">';
                $h .= json_encode($scheme);
                $h .= '</script>';
                $content .= $h;

                if (!function_exists('is_amp_endpoint') || !is_amp_endpoint()) {
                    wp_register_script('pf_rating', plugin_dir_url(__FILE__) . 'js/jquery.star-rating-svg.min.js', array('jquery'), null, false);
                    wp_register_script('pf_rating_js', plugin_dir_url(__FILE__) . 'js/rating.js', array('jquery', 'pf_rating'), microtime(), true);
                    wp_register_style('pf_rating', plugin_dir_url(__FILE__) . 'css/star-rating-svg.css');
                    wp_register_style('pf_rating_css', plugin_dir_url(__FILE__) . 'css/ratings.css');

                    wp_enqueue_script('pf_rating');
                    wp_enqueue_script('pf_rating_js');
                    wp_enqueue_style('pf_rating');
                    wp_enqueue_style('pf_rating_css', microtime());

                    $h = "<div class='pf-rating' itemprop='aggregateRating' itemscope='' itemtype='http://schema.org/AggregateRating'>";

                    $h .= $options['sse_text_before'];

                    $h .= "<div class='my-rating' data-post-id='{$postID}'  data-blog-id='{$blogID}' data-rating='{$o->average}' data-counter='{$o->count}' data-api='{$ep}' data-high-demand='{$highdemand}'></div>";
                    $h .= "<div>Avaliação média: <span id='ratingaverage' itemprop='ratingValue'>{$o->average}</span><br />Total de Votos: <span id='ratingcount' itemprop='ratingCount'>{$o->count}</span></div>";
                    $h .= "<meta itemprop='bestRating' content='5'>";
                    $h .= "<meta itemprop='worstRating' content='1'>";
                    $h .= "<div itemprop='itemReviewed' itemscope='' itemtype='http://schema.org/CreativeWorkSeries'></div>";

                    $h .= "<div class='rating-title' itemprop='name'>" . get_the_title() . "</div>";

                    $h .= "</div>";
                    $content .= $h;
                }
            }
        }
    }

    return $content;
}

add_filter('the_content', 'pfRating', 1);

function sse_add_settings_link($links)
{
    $settings_link = '<a href="options-general.php?page=sobe_sobe_estrelinha">' . __('Settings') . '</a>';
    array_push($links, $settings_link);
    array_push($links, '<a href="https://lucrandonarede.com.br/avaliacoes-com-estrelinhas-feitas-do-jeito-certo/" target="_blank">Manual</a>');
    return $links;
}
$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'sse_add_settings_link');

add_action('admin_menu', 'sse_add_admin_menu');
add_action('admin_init', 'sse_settings_init');

function sse_add_admin_menu()
{

    add_options_page('Sobe Sobe Estrelinha', 'Sobe Sobe Estrelinha', 'manage_options', 'sobe_sobe_estrelinha', 'sse_options_page');

}

function sse_settings_init()
{

    register_setting('sse_pluginPage', 'sse_settings');

    add_settings_section(
        'sse_sse_pluginPage_section',
        __('Configurações Gerais', 'wordpress'),
        'sse_settings_section_callback',
        'sse_pluginPage'
    );

    add_settings_field(
        'sse_api_endpoint',
        __('API Endpoint', 'wordpress'),
        'sse_api_endpoint_render',
        'sse_pluginPage',
        'sse_sse_pluginPage_section'
    );

    add_settings_field(
        'sse_show_in_posts',
        __('Estrelinhas nos posts', 'wordpress'),
        'sse_show_in_posts_render',
        'sse_pluginPage',
        'sse_sse_pluginPage_section'
    );

    add_settings_field(
        'sse_show_in_pages',
        __('Estrelinhas nas páginas', 'wordpress'),
        'sse_show_in_pages_render',
        'sse_pluginPage',
        'sse_sse_pluginPage_section'
    );

    add_settings_field(
        'sse_text_before',
        __('Texto antes das notas', 'wordpress'),
        'sse_text_before_render',
        'sse_pluginPage',
		'sse_sse_pluginPage_section'
    );

	add_settings_field(
        'sse_highdemand',
        __('Modo de alta demanda', 'wordpress'),
        'sse_highdemand',
        'sse_pluginPage',
        'sse_sse_pluginPage_section'
    );
}

function sse_text_before_render()
{

    $options = get_option('sse_settings');
    ?>
    <input type='text' name='sse_settings[sse_text_before]' value='<?php echo $options['sse_text_before']; ?>'>
    <?php

}

function sse_api_endpoint_render()
{

    $options = get_option('sse_settings');
    ?>
    <input type='text' name='sse_settings[sse_api_endpoint]' value='<?php echo $options['sse_api_endpoint']; ?>'> Peça ao seu fornecedor do plugin!
    <?php

}

function sse_show_in_posts_render()
{

    $options = get_option('sse_settings');
    ?>
    <input type='checkbox' name='sse_settings[sse_show_in_posts]' <?php checked($options['sse_show_in_posts'], 1);?> value='1'> (Recomendado)
    <?php

}

function sse_show_in_pages_render()
{

    $options = get_option('sse_settings');
    ?>
    <input type='checkbox' name='sse_settings[sse_show_in_pages]' <?php checked($options['sse_show_in_pages'], 1);?> value='1'> (Não recomendado)
    <?php

}

function sse_highdemand_render()
{

    $options = get_option('sse_highdemand');
    ?>
    <input type='checkbox' name='sse_settings[sse_highdemand]' <?php checked($options['sse_highdemand'], 1);?> value='1'> (Recomendado)
    <?php

}

function sse_settings_section_callback()
{

    echo __('Personalize as estrelinhas', 'wordpress');

}

function sse_options_page()
{

    ?>
    <form action='options.php' method='post'>

        <h1>Sobe Sobe Estrelinha</h1>

        <?php
settings_fields('sse_pluginPage');
    do_settings_sections('sse_pluginPage');
    submit_button();
    ?>

    </form>
    <?php
$value = get_transient(TRANSIENT_SITE_ID);
    if ($value) {
        $options = get_option('sse_settings');
        $av = $options['sse_api_endpoint'] . '/autovote/' . $value;
        echo "<p>URL de voto automático: <a href='$av' target='_blank'>$av</a></p>";
        echo "<p><a href='https://lucrandonarede.com.br/avaliacoes-com-estrelinhas-feitas-do-jeito-certo/'>Manual do plugin</a></p>";
    }
}

// Create the function to retrieve custom fields value
function sse_get_custom_field($value)
{
    global $post;

    $custom_field = get_post_meta($post->ID, $value, true);
    if (!empty($custom_field)) {
        return is_array($custom_field) ? stripslashes_deep($custom_field) : stripslashes(wp_kses_decode_entities($custom_field));
    }

    return false;
}

// Register the Metabox for Post Type
function sse_add_meta_box()
{

    add_meta_box('sse-meta-box', __('Sobe Sobe Estrelinha', 'sse'), 'sse_meta_box_output', array('post', 'page'), 'side', 'high'); //show in custom post
}
add_action('add_meta_boxes', 'sse_add_meta_box');

// Metabox Output
function sse_meta_box_output($post)
{
    // create a wp nonce field
    wp_nonce_field('sse_nonce', 'my_nonce_sse');
    $value = get_post_meta($post->ID, 'sse_override', true); //my_key is a meta_key. Change it to whatever you want
    if ($value != 'S' and $value != 'N') {
        $value = 'X';
    }
    ?>
    <p><input type="radio" name="sse_override_radio" value="X" <?php checked($value, 'X');?> >Usar configuração global</p>
    <p><input type="radio" name="sse_override_radio" value="S" <?php checked($value, 'S');?> >Forçar estrelinhas</p>
    <p><input type="radio" name="sse_override_radio" value="N" <?php checked($value, 'N');?> >Ocultar estrelinhas</p>
    <?php
}

// Save the Metabox values with Post ID
function sse_save_metabox($post_id)
{
// Stop the script when doing autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

// Verify the nonce. If is not there, stop the script
    if (!isset($_POST['my_nonce_sse']) || !wp_verify_nonce($_POST['my_nonce_sse'], 'sse_nonce')) {
        return;
    }

// Stop the script if the user does not have edit permissions
    if (!current_user_can('edit_post')) {
        return;
    }

// Save the Contributor Name Field
    if (isset($_POST['sse_override_radio'])) {
        update_post_meta($post_id, 'sse_override', esc_attr($_POST['sse_override_radio']));
    }

}
add_action('save_post', 'sse_save_metabox');
